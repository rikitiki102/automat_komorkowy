#ifndef _ZAPIS_H_
#define _ZAPIS_H_
#include "dane.h"

Siatka inicjujsiatke(int r, int c);

Siatka odczyt(char *fname);

void wypisz_plik_png(char * file_name);

void przetworz_plik(Siatka siatka);

#endif

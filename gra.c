#include <stdio.h>
#include <stdlib.h>
#include "dane.h"
#include "gra.h"
#include "zapis.h"

void inicjujkom(Komorka kom, int w, int c, int s){
	kom->rn =w;
	kom->cn =c;
	kom->stan = s;
}


/* funkcja do określania sąsiedztwa  */
Sasiedzi sasiedztwo(Komorka kom, Siatka siatka){ 
        int i,j,r, c ;
	Sasiedzi dane = (Sasiedzi)malloc(sizeof(Sasiedzi));  
	dane->zywe = 0;
	dane->martwe = 0;
	r = kom->rn;
	c = kom->cn;
	if(siatka->tab[r-1][c-1] == 1)
		dane->zywe++;
	if(siatka->tab[r-1][c] == 1)
		dane->zywe++;
	if(siatka->tab[r-1][c+1] == 1)
		dane->zywe++;
	if(siatka->tab[r][c-1] == 1)
		dane->zywe++;
	if(siatka->tab[r][c+1] == 1)
		dane->zywe++;
	if(siatka->tab[r+1][c-1] == 1)
		dane->zywe++;
	if(siatka->tab[r+1][c] == 1)
		dane->zywe++;
	if(siatka->tab[r+1][c+1] == 1)
		dane->zywe++;
	return dane;
}

/* funkcja do kopiowania siatki */

Siatka copyboard(Siatka siatka){
	int i, j;
	Siatka siatka2 = NULL;
	siatka2 = inicjujsiatke(siatka->r,siatka->c);
	for(i = 0; i < siatka->r; i++)
		for(j =0; j < siatka->c ; j++)
			siatka2->tab[i][j] = siatka->tab[i][j];
	return siatka2;
}		

			
/* funkcja ustalająca stan komorki na podstawie reguł */

Komorka reguly(Komorka kom, Sasiedzi dane){
	if(kom->stan == 0)
		if(dane->zywe == 3)
			kom->stan = 1;
	if(kom->stan == 1){
		if(dane->zywe == 2 || dane->zywe == 3)
			kom->stan = 1;
		else
			kom->stan = 0;
	}
	return kom;
}

/* funkcja sterujaca tworzaca jedna generacje */
/* zrobic druga siatke i do niej zapisywac kom zeby nie dzialalalo na jednej siatce */
/* bo czyta komorki ze zmienionym stanem z wyzszych rzedow */

Siatka gra(Siatka siatka){
	int i, j;
	Komorka kom = (Komorka) malloc(sizeof(Komorka)) ;
	Sasiedzi otoczenie = (Sasiedzi)malloc(sizeof(Sasiedzi));
	Siatka siatka2 = copyboard(siatka);
	for(i = 1; i < (siatka->r)-1; i++)
		for(j =1 ; j < siatka->c-1; j++){
			if(siatka->tab[i][j] == 1 ){
				inicjujkom(kom, i, j, 1);
				otoczenie = sasiedztwo(kom, siatka);
				kom = reguly(kom, otoczenie);		
				siatka2->tab[i][j] = kom->stan;
			}
			if(siatka->tab[i][j] == 0) {
				inicjujkom(kom,i,j,0);
				otoczenie = sasiedztwo(kom, siatka);
				kom - reguly(kom, otoczenie);
				siatka2->tab[i][j] = kom->stan;
			}
		}
	return siatka2;
}


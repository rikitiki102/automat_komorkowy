aut: gra.o zapis.o main.o
	$(CC) -o aut gra.o zapis.o main.o -lpng
zapis.o: zapis.c
	$(CC) -c zapis.c -lpng
gra.o: gra.c
	$(CC) -c gra.c
main.o: main.c
	$(CC) -c main.c


.PHONY : clean
clean:
	rm -r *.o


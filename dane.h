#ifndef _DANE_H_
#define _DANE_H_

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <png.h>
#include <getopt.h>

int x, y;

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
png_bytep * row_pointers;


/* struktura komorki */
typedef struct kom{
	int rn;
	int cn;
	int stan;
} *Komorka;

/*struktura siatki*/
typedef struct siatka{
	int **tab;
	int r;
	int c;
} *Siatka;

/*przechowywanie danych o sasiadach*/

typedef struct sasiedzi{
	int zywe;
	int martwe;
} *Sasiedzi;

#endif

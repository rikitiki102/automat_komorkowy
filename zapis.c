#include "zapis.h"
#include "dane.h"
#include "gra.h"
#define SCALE 3

/* funkcja inicjujaca siatke */

Siatka inicjujsiatke(int r, int c) {
        int i, j;
        Siatka siatka = (Siatka)malloc(sizeof(Siatka));
        if (siatka != NULL) {
                siatka->r = r;
                siatka->c = c;
                siatka->tab = (int**) malloc(sizeof(int*) * r);
                for (i=0; i < r; i++) {
                        siatka->tab[i] = (int*)malloc(sizeof(int) * c);
                }
        }
        for(i =0 ; i < r; i++)
                for(j =0 ; j < c; j++)
                        siatka->tab[i][j] = 9;
        return siatka;
}


/* funkcja do odczytywania */

Siatka odczyt(char * fname) {
 	int r,c;
    	int ir, ic;
  	FILE * fin =  fopen(fname,"r");
	Siatka siatka = NULL;
    	if (fin != NULL) {
        	fscanf(fin,"%d %d",&r,&c);
             	siatka = inicjujsiatke(r+2,c+2);
     	if (siatka != NULL) {
               	for (ir = 1; ir < r+1; ir++)
                 	for (ic = 1; ic < c+1; ic++)
                   		fscanf(fin, "%d",&(siatka->tab[ir][ic]));
  	} else {
       		fprintf(stderr,"Wystapil problem podczas tworzenia siatki o rozmiarach %d x %d dla danych z pliku: %s\n", r, c, fname);
   	}
      	fclose(fin);
  	} else {
          	fprintf(stderr,"Nie podano pliku z początkowa konfiguracja %s\n", fname);
		exit(EXIT_FAILURE);
  	}   
  	return siatka;
}

void przetworz_plik(Siatka siatka) {
  	width = SCALE* siatka->c;
  	height = SCALE* siatka->r;
  	bit_depth = 8;
  	color_type = PNG_COLOR_TYPE_GRAY;
  	number_of_passes = 1;
  	row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
  	for (y=0; y<height; y++)
    		row_pointers[y] = (png_byte*) malloc(sizeof(png_byte) * width);
  	for (y=0; y<height; y+=SCALE) {
			png_byte* row = row_pointers[y];
			png_byte* row1 = row_pointers[y+1];
			png_byte* row2 = row_pointers[y+2];
 		for(x=0; x<width; x+=SCALE) {
			if(siatka->tab[y/3][x/3] == 1){
					row[x] =  255;
					row[x+1] = 255;
					row[x+2] = 255;
					row1[x] = 255;
					row1[x+1] = 255;
					row1[x+2] = 255;
					row2[x] = 255;
					row2[x+1] = 255;
					row2[x+2] = 255;
			}
			if(siatka->tab[y/3][x/3] == 0 || siatka->tab[y/3][x/3] == 9) {
					row[x] = 0;
					row[x+1] = 0;
					row[x+2] = 0;
					row1[x] = 0;
					row1[x+1] = 0;
					row1[x+2] = 0;
					row2[x] = 0;
					row2[x+1] = 0;
					row2[x+2] = 0;
			}
    		}
	}  	
}

void wypisz_plik_png(char* file_name) {
  	FILE *fp = fopen(file_name, "wb");
  	if (!fp)
    		printf("[write_png_file] File %s could not be opened for writing", file_name);
  	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  	if (!png_ptr)
    		printf("[write_png_file] png_create_write_struct failed");
  	info_ptr = png_create_info_struct(png_ptr);
  	if (!info_ptr)
    		printf("[write_png_file] png_create_info_struct failed");
  	if (setjmp(png_jmpbuf(png_ptr)))
    		printf("[write_png_file] Error during init_io");
  	png_init_io(png_ptr, fp);
  	if (setjmp(png_jmpbuf(png_ptr)))
    		printf("[write_png_file] Error during writing header");
  	png_set_IHDR(png_ptr, info_ptr, width, height,
   	bit_depth, color_type, PNG_INTERLACE_NONE,
   	PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  	png_write_info(png_ptr, info_ptr);
  	if (setjmp(png_jmpbuf(png_ptr)))
    		printf("[write_png_file] Error during writing bytes");
  	png_write_image(png_ptr, row_pointers);
  	if (setjmp(png_jmpbuf(png_ptr)))
    		printf("[write_png_file] Error during end of write");
  	png_write_end(png_ptr, NULL);
  	for (y=0; y<height; y++)
    		free(row_pointers[y]);
  	free(row_pointers);
	fclose(fp);
}


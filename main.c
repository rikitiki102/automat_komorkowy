#include "zapis.h"
#include "dane.h"
#include "gra.h"

int main(int argc, char **argv){
	int i;
  	int opt;
	int j =0;
  	char *plikwe = NULL;
  	char *plikwy = NULL;
	char naz[40];
	int nr = 1;
	char *konc = ".png";
 	int n = 100;
	int p = 20;
	char *nazwaprog= argv[0];
	char *usage = "Stosowanie: -n [liczba generacji] -p [liczba plikow do powstania]\n"
		      "            -f [plik z konf. poczatkowa] -t [nazwa pliku wyjsciowego]\n";
	Siatka siatka;
  	while ((opt = getopt (argc, argv, "n:p:f:t:")) != -1) {
    		switch (opt) {
    		case 'n':
      			n = atoi(optarg);
      			break;
    		case 'p':
      			p = atoi(optarg);
      			break;
    		case 'f':
      			plikwe = optarg;
      			break;
    		case 't':
     			plikwy = optarg;
      		break;
   		default:                  
      		fprintf (stderr, usage, nazwaprog);
      		exit (EXIT_FAILURE);
    		}
	}

	if( optind < argc ) {
		fprintf( stderr, "\nBad parameters!\n" );
		for( ; optind < argc; optind++ )
			fprintf( stderr, "\t\"%s\"\n", argv[optind] );
		fprintf( stderr, "\n" );
		fprintf( stderr, usage, nazwaprog );
		exit( EXIT_FAILURE );
	}	
	siatka = odczyt(plikwe);		
	for(i = 0; i < n ; i++){
		siatka = gra(siatka);
		if(j < p ) {
			przetworz_plik(siatka);
			sprintf(naz, "%s%d%s",plikwy, nr, konc);
			wypisz_plik_png(naz);
			nr++;
			j++;
		}
	}		
	return 0;
}		
